package com.jn.eureka_currencyproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaCurrencyprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaCurrencyprojectApplication.class, args);
    }

}
